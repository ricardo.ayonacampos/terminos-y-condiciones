import 'package:flutter/material.dart';
import 'BackTerminos.dart';

class Header extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Widget textSection = Container(
      padding: const EdgeInsets.all(28),
      child: Text(
        '1. INTRODUCCIÓN El presente Acuerdo es un contrato entre usted (en lo sucesivo EL USUARIO) y Tulkit 3.0 S.A.P.I DE C.V.'
    'para la utilización de la aplicación móvil denominada TULKIT PAY (en adelante denominado como “TULKIT”)'
    'El uso de TULKIT, implica que usted debe aceptar todos los términos y condiciones contenidos en este'
    'Acuerdo incluido el aviso de privacidad. Para tal efecto, usted debe leer atentamente este Acuerdo.',
        style: const TextStyle(
            fontSize: 15.0,
            color: Color(0xFF424242)
        ),
        softWrap: true,
      ),
    );

    return new Scaffold(
      body: new Stack(
        children: <Widget>[
          new BackTerminos(),
          new Container(
            alignment: Alignment.center,
            margin: new EdgeInsets.only(
                top: 50.0
            ),
            child: new Column(
              children: <Widget>[
                new Text(
                  "Terminos y Condiciones",
                  style: const TextStyle(
                      fontSize: 25.0,
                      color: Color(0xFF424242),
                      fontWeight: FontWeight.w600
                  ),
                ),
                textSection
              ],

            ),
          )
        ],
      ),
    );
  }

}